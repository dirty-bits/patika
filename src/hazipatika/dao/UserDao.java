package hazipatika.dao;

import hazipatika.model.User;
import java.sql.*;

public class UserDao extends BaseDao {

    public static void saveUser(User user) throws SQLException {
        PreparedStatement stmt;

        try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password)) {

            String sqlStatement = "INSERT INTO felhasznalo (email, jelszo, veznev, knev) VALUES(?, ?, ?, ?)";

            stmt = conn.prepareStatement(sqlStatement);
            stmt.setString(1, user.getEmail());
            stmt.setString(2, user.getPassw());
            stmt.setString(3, user.getLastName());
            stmt.setString(4, user.getFirstName());

            stmt.executeUpdate();
        }
    }

    public static boolean checkLogin(User user) throws SQLException {
        boolean isOk = false;

        try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password)) {
            Statement stmt = conn.createStatement();

            String sqlStatement = "SELECT email, jelszo FROM felhasznalo";

            ResultSet rs = stmt.executeQuery(sqlStatement);

            while (rs.next()) {
                if (user.getEmail().equals(rs.getString("email"))
                        && user.getPassw().equals(rs.getString("jelszo"))) {
                    isOk = true;
                }
            }

        }

        return isOk;
    }

    public static User findUserByEmailAndPassword(User user) throws SQLException {
        User newUser = new User();

        try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password)) {
            Statement stmt = conn.createStatement();

            String sqlStatement = "SELECT email, jelszo, veznev, knev, id FROM felhasznalo";

            ResultSet rs = stmt.executeQuery(sqlStatement);

            while (rs.next()) {
                if (user.getEmail().equals(rs.getString("email"))
                        && user.getPassw().equals(rs.getString("jelszo"))) {
                    newUser.setEmail(rs.getString("email"));
                    newUser.setPassw(rs.getString("jelszo"));
                    newUser.setFirstName(rs.getString("knev"));
                    newUser.setLastName(rs.getString("veznev"));
                    newUser.setId(rs.getInt("id"));
                }
            }

        }

        return newUser;
    }
}

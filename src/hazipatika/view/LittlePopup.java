package hazipatika.view;

import java.awt.event.KeyEvent;
import javax.swing.JFrame;

public class LittlePopup extends BaseView {

    private JFrame frameToOpen;

    public LittlePopup(String title, String text) {
        initComponents();
        centeredFrame(this);
        setText(title, text);
    }

    public LittlePopup(String title, String text, JFrame frameToOpen) {
        initComponents();
        centeredFrame(this);
        setText(title, text);
        this.frameToOpen = frameToOpen;
    }

    private void setText(String title, String text) {
        label_label.setText(title);
        jTextArea1.setText(text);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel_full = new javax.swing.JPanel();
        jButton_ok = new javax.swing.JButton();
        label_label = new java.awt.Label();
        jScrollPane_scroll = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel_full.setBackground(new java.awt.Color(255, 255, 255));
        jPanel_full.setEnabled(false);

        jButton_ok.setBackground(new java.awt.Color(255, 255, 255));
        jButton_ok.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jButton_ok.setForeground(new java.awt.Color(0, 123, 255));
        jButton_ok.setText("Rendben");
        jButton_ok.setBorder(null);
        jButton_ok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_okActionPerformed(evt);
            }
        });

        label_label.setAlignment(java.awt.Label.CENTER);
        label_label.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        label_label.setForeground(new java.awt.Color(0, 123, 255));
        label_label.setText("Lejáró gyógyszerek");

        jScrollPane_scroll.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane_scroll.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(5);
        jTextArea1.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        jTextArea1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextArea1KeyReleased(evt);
            }
        });
        jScrollPane_scroll.setViewportView(jTextArea1);

        javax.swing.GroupLayout jPanel_fullLayout = new javax.swing.GroupLayout(jPanel_full);
        jPanel_full.setLayout(jPanel_fullLayout);
        jPanel_fullLayout.setHorizontalGroup(
                jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel_fullLayout.createSequentialGroup()
                                .addGroup(jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel_fullLayout.createSequentialGroup()
                                                .addGap(200, 200, 200)
                                                .addComponent(jButton_ok, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                        .addComponent(label_label, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
                        .addGroup(jPanel_fullLayout.createSequentialGroup()
                                .addGap(90, 90, 90)
                                .addComponent(jScrollPane_scroll, javax.swing.GroupLayout.PREFERRED_SIZE, 338, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(100, Short.MAX_VALUE))
        );
        jPanel_fullLayout.setVerticalGroup(
                jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_fullLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(label_label, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane_scroll, javax.swing.GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton_ok, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel_full, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel_full, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_okActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_okActionPerformed

        if (frameToOpen != null) {
            frameToOpen.setVisible(true);
        }

        this.dispose();
    }//GEN-LAST:event_jButton_okActionPerformed

    private void jTextArea1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextArea1KeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jButton_ok.doClick();
        }
    }//GEN-LAST:event_jTextArea1KeyReleased

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LittlePopup.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LittlePopup.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LittlePopup.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LittlePopup.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LittlePopup("", "").setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_ok;
    private javax.swing.JPanel jPanel_full;
    private javax.swing.JScrollPane jScrollPane_scroll;
    private javax.swing.JTextArea jTextArea1;
    private java.awt.Label label_label;

    // End of variables declaration//GEN-END:variables
}

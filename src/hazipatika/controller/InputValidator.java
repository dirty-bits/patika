package hazipatika.controller;

import java.awt.*;
import javax.swing.*;
import hazipatika.view.errors.CheckCorrection;

public class InputValidator {

    private static void inputErrorMessage(JLabel label, String message) {
        label.setForeground(Color.ORANGE);
        label.setText(message);
        label.setVisible(true);
    }

    public static boolean isPasswordValid(JPasswordField passwordField, JLabel label) {
        String password = new String(passwordField.getPassword());

        if (!CheckCorrection.isPassword(password)) {
            inputErrorMessage(label, "Legalább 8 karakter szükséges!");
            return false;
        }

        return true;

    }

    public static boolean isEmailValid(String email, JLabel label) {
        if (!CheckCorrection.isEmail(email)) {
            inputErrorMessage(label, "Helytelen e-mail cím!");
            return false;
        }

        return true;
    }

    public static boolean isNameValid(String name, JLabel label) {
        if (!CheckCorrection.isName(name)) {
            inputErrorMessage(label, "Helytelen név!");
            return false;
        }

        return true;
    }
}

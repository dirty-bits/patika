package hazipatika.model;

import java.util.Date;

public class Medicine {

    private String iconPath;

    private String name;

    private String description;

    private int avgDoses;

    private Date expDate;

    private int currentPieces;

    private int id;

    public Medicine(String iconPath, String name, int avgDoses, Date expDate, String description,
                    int currentPieces) {
        this.iconPath = iconPath;
        this.name = name;
        this.avgDoses = avgDoses;
        this.expDate = expDate;
        this.currentPieces = currentPieces;
        this.description = description;
    }

    public Medicine() {
        this.iconPath = "src/images/pics/default.jpeg";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String path) {
        this.iconPath = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAvgDoses() {
        return avgDoses;
    }

    public void setAvgDoses(int avgDoses) {
        this.avgDoses = avgDoses;
    }

    public Date getExpDate() {
        return expDate;
    }

    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }
    
    public int getCurrentPieces() {
        return currentPieces;
    }

    public void setCurrentPieces(int currentPieces) {
        this.currentPieces = currentPieces;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}

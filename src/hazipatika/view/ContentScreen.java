package hazipatika.view;

import hazipatika.dao.MedicineDao;
import hazipatika.model.Medicine;
import hazipatika.model.User;
import javax.swing.*;
import static javax.swing.SwingUtilities.isLeftMouseButton;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ContentScreen extends javax.swing.JFrame {

    static DefaultTableModel tableModel;

    private static List<Integer> idList = new ArrayList<>();

    private User user = new User();
    
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Never called, only from main from this class!!!
     */
    public ContentScreen() {

        initComponents();

        tableModel = (DefaultTableModel) jTable_table.getModel();
        jTable_table.getColumnModel().getColumn(0).setPreferredWidth(400);

        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    /**
     * Called after login succeeded
     * @param user User who is logged in
     */
    public ContentScreen(User user) {

        initComponents();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);

        tableModel = (DefaultTableModel) jTable_table.getModel();
        jTable_table.getColumnModel().getColumn(0).setPreferredWidth(400);
        jTable_table.getColumnModel().getColumn(0).setMaxWidth(500);
        jTable_table.getColumnModel().getColumn(0).setMinWidth(200);
        this.user = user;
        setLabel();

        updateTable(this.user);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton_newItem = new javax.swing.JButton();
        jButton_removeSelectedItem = new javax.swing.JButton();
        jButton_modify = new javax.swing.JButton();
        jButton_logout = new javax.swing.JButton();
        jLabel_username = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable_table = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 123, 255));

        jButton_newItem.setBackground(new java.awt.Color(0, 51, 153));
        jButton_newItem.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jButton_newItem.setForeground(new java.awt.Color(255, 255, 255));
        jButton_newItem.setText("Új gyógyszer hozzáadása");
        jButton_newItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_newItemActionPerformed(evt);
            }
        });

        jButton_removeSelectedItem.setBackground(new java.awt.Color(0, 51, 153));
        jButton_removeSelectedItem.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jButton_removeSelectedItem.setForeground(new java.awt.Color(255, 255, 255));
        jButton_removeSelectedItem.setText("Kiválasztott gyógyszer törlése");
        jButton_removeSelectedItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_removeSelectedItemActionPerformed(evt);
            }
        });

        jButton_modify.setBackground(new java.awt.Color(0, 51, 153));
        jButton_modify.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jButton_modify.setForeground(new java.awt.Color(255, 255, 255));
        jButton_modify.setText("Kiválasztott gyógyszer módosítása");
        jButton_modify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_modifyActionPerformed(evt);
            }
        });

        jButton_logout.setBackground(new java.awt.Color(0, 51, 153));
        jButton_logout.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jButton_logout.setForeground(new java.awt.Color(255, 255, 255));
        jButton_logout.setText("Kijelentkezés");
        jButton_logout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_logoutActionPerformed(evt);
            }
        });

        jLabel_username.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        jLabel_username.setForeground(new java.awt.Color(255, 255, 255));
        jLabel_username.setText("Üdvözöljük ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel_username, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton_newItem)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton_modify)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton_removeSelectedItem)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton_logout, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(13, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton_logout, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton_removeSelectedItem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton_modify, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton_newItem, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel_username))
                .addContainerGap())
        );

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(null);
        jScrollPane1.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N

        jTable_table.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jTable_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Név", "Átlagos heti adagolás", "Lejárati dátum", "Meglévő darabszám"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable_table.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTable_table.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jTable_table.setGridColor(new java.awt.Color(0, 123, 255));
        jTable_table.setRowHeight(50);
        jTable_table.setSelectionBackground(new java.awt.Color(153, 204, 255));
        jTable_table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTable_table.getTableHeader().setReorderingAllowed(false);
        jTable_table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable_tableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable_table);
        if (jTable_table.getColumnModel().getColumnCount() > 0) {
            jTable_table.getColumnModel().getColumn(0).setMinWidth(50);
            jTable_table.getColumnModel().getColumn(0).setPreferredWidth(50);
            jTable_table.getColumnModel().getColumn(0).setMaxWidth(150);
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1164, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 446, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTable_tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable_tableMouseClicked

        if (isLeftMouseButton(evt) && evt.getClickCount() == 2) {

            MedicinePopup popupScreen = new MedicinePopup(this.user, MedicinePopup.ModifyType.MODIFY, idList.get(jTable_table.getSelectedRow()));
            popupScreen.setVisible(true);
            
            setButton(evt);

            try {
                int selectedId = idList.get(jTable_table.getSelectedRow());
                popupScreen.getjTextField_medicineName()
                        .setText((String) tableModel.getValueAt(jTable_table.getSelectedRow(), 0));
                popupScreen.getjTextField_medicineAvgDosage()
                        .setText(Integer.toString((Integer) tableModel.getValueAt(jTable_table.getSelectedRow(), 1)));
                popupScreen.getjTextField_medicineExpDate()
                        .setText((String) tableModel.getValueAt(jTable_table.getSelectedRow(), 2));
                popupScreen.getjSpinner_medicinePieces()
                        .setValue(tableModel.getValueAt(jTable_table.getSelectedRow(), 3));
                ImageIcon ii = new ImageIcon(MedicineDao.findImagePathById(selectedId));
                popupScreen.getjLabel_medicinePhoto().setIcon(new javax.swing.ImageIcon(ii.getImage()
                        .getScaledInstance(174, 159, Image.SCALE_DEFAULT)));
            } catch (SQLException e) {
                Logger.getLogger(Medicine.class.getName()).log(Level.SEVERE, null, e);
            }

        }
    }//GEN-LAST:event_jTable_tableMouseClicked

    private void jButton_logoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_logoutActionPerformed

        JFrame openLoginScreen = new LoginSreen();
        openLoginScreen.setVisible(true);
        this.dispose();

    }//GEN-LAST:event_jButton_logoutActionPerformed

    private void jButton_modifyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_modifyActionPerformed

        if (jTable_table.getSelectedRow() == -1) {
            LittlePopup littlePopup = new LittlePopup(Constants.ERROR_STRING, Constants.NO_SELECTED_ROW);
            littlePopup.setVisible(true);
            return;
        }
        
        MedicinePopup popupScreen = new MedicinePopup(this.user, MedicinePopup.ModifyType.MODIFY, idList.get(jTable_table.getSelectedRow()));
        popupScreen.setVisible(true);

        try {
            int selectedId = idList.get(jTable_table.getSelectedRow());
            popupScreen.getjTextField_medicineName()
                    .setText((String) tableModel.getValueAt(jTable_table.getSelectedRow(), 0));
            popupScreen.getjTextField_medicineAvgDosage()
                    .setText(Integer.toString((Integer) tableModel.getValueAt(jTable_table.getSelectedRow(), 1)));
            popupScreen.getjTextField_medicineExpDate()
                    .setText(sdf.format(tableModel.getValueAt(jTable_table.getSelectedRow(), 2)));
            popupScreen.getjSpinner_medicinePieces()
                    .setValue(tableModel.getValueAt(jTable_table.getSelectedRow(), 3));
            popupScreen.getjTextArea_description().setText(MedicineDao.findById(selectedId).getDescription());
            ImageIcon ii = new ImageIcon(MedicineDao.findImagePathById(selectedId));
            popupScreen.getjLabel_medicinePhoto().setIcon(new javax.swing.ImageIcon(ii.getImage()
                    .getScaledInstance(174, 159, Image.SCALE_DEFAULT)));
        } catch (SQLException e) {
            Logger.getLogger(Medicine.class.getName()).log(Level.SEVERE, null, e);
        }


    }//GEN-LAST:event_jButton_modifyActionPerformed

    private void jButton_removeSelectedItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_removeSelectedItemActionPerformed
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(null,
                "Szeretné törölni a kiválasztott elemet?", "Warning", dialogButton);

        if (dialogResult == JOptionPane.YES_OPTION) {

            if (this.jTable_table.getSelectedRow() == -1) {
                LittlePopup littlePopup = new LittlePopup(Constants.ERROR_STRING, Constants.NO_SELECTED_ROW);
                littlePopup.setVisible(true);
            }

            removeItem();

        }

    }//GEN-LAST:event_jButton_removeSelectedItemActionPerformed

    private void jButton_newItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_newItemActionPerformed
        MedicinePopup popupScreen = new MedicinePopup(this.user, MedicinePopup.ModifyType.ADD);
        popupScreen.setVisible(true);

    }//GEN-LAST:event_jButton_newItemActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ContentScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ContentScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ContentScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ContentScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ContentScreen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_logout;
    private javax.swing.JButton jButton_modify;
    private javax.swing.JButton jButton_newItem;
    private javax.swing.JButton jButton_removeSelectedItem;
    private javax.swing.JLabel jLabel_username;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable_table;
    // End of variables declaration//GEN-END:variables

    private void removeItem() {

        try {
            MedicineDao.removeMedicine((String) tableModel.getValueAt(this.jTable_table.getSelectedRow(), 0));
            tableModel.removeRow(this.jTable_table.getSelectedRow());
        } catch (SQLException e) {
            LittlePopup lp = new LittlePopup("Sikertelen kapcsolat az adatbázissal!", "sqlex");
            lp.setVisible(true);
            Logger.getLogger(Medicine.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    static void updateTable(User user) {
        try {
            int rowCount = tableModel.getRowCount();
            for (int i = rowCount - 1; i >= 0; i--) {
                tableModel.removeRow(i);
            }

            idList = MedicineDao.refreshTable(tableModel, user);
        } catch (SQLException e) {
            LittlePopup lp = new LittlePopup("Sikertelen kapcsolat az adatbázissal!", "sqlex");
            lp.setVisible(true);
            Logger.getLogger(Medicine.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private void setLabel() {
        jLabel_username.setText(jLabel_username.getText() + user.getFirstName() + "!");
    }

    private void setButton(MouseEvent evt) {
        
    }

}

package hazipatika.view.errors;

public class CheckCorrection {

    public static boolean isEmail(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    public static boolean isPassword(String password) {
        int length = password.length();
        return length > 7 && length < 31;
    }

    public static boolean isName(String name) {
        int length = name.length();
        return length > 1 && length < 21;
    }

    public static boolean isDose(String dose) {
        String regex = "^[0-9]*[0-9]";
        return dose.matches(regex);
    }
}

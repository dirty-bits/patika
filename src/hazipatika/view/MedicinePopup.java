package hazipatika.view;

import hazipatika.dao.MedicineDao;
import hazipatika.view.errors.CheckCorrection;
import hazipatika.model.Medicine;
import hazipatika.model.User;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;

public class MedicinePopup extends javax.swing.JFrame {

    private Medicine medicine;

    private User user;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private ModifyType modifyType;

    public enum ModifyType {
        ADD,
        MODIFY
    }

    public MedicinePopup() {
    }

    public MedicinePopup(User user, ModifyType modifyType) {
        initComponents();
        medicine = new Medicine();
        LoginSreen.centeredFrame(this);
        this.user = user;
        this.modifyType = modifyType;
        setTextOnButton(this.modifyType);
    }
    
    public MedicinePopup(User user, ModifyType modifyType, int medId) {
        initComponents();
        medicine = new Medicine();
        LoginSreen.centeredFrame(this);
        this.user = user;
        this.modifyType = modifyType;
        setTextOnButton(this.modifyType);
        medicine.setId(medId);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel_full = new javax.swing.JPanel();
        jTextField_medicineName = new javax.swing.JTextField();
        jTextField_medicineAvgDosage = new javax.swing.JTextField();
        jTextField_medicineExpDate = new javax.swing.JTextField();
        jButton_PicUp = new javax.swing.JButton();
        jPanel_medicineImage = new javax.swing.JPanel();
        jLabel_medicinePhoto = new javax.swing.JLabel();
        jSpinner_medicinePieces = new javax.swing.JSpinner();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea_description = new javax.swing.JTextArea();
        jLabel_error_name = new javax.swing.JLabel();
        jLabel_error_avgDoses = new javax.swing.JLabel();
        jLabel_error_expDate = new javax.swing.JLabel();
        jLabel_error_currentPieces = new javax.swing.JLabel();
        jButton_AddOrSave = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setBackground(new java.awt.Color(255, 255, 255));
        setResizable(false);
        setType(java.awt.Window.Type.POPUP);

        jPanel_full.setBackground(new java.awt.Color(255, 255, 255));
        jPanel_full.setToolTipText("");

        jTextField_medicineName.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jTextField_medicineName.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Gyógyszer neve", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 18), new java.awt.Color(0, 123, 255))); // NOI18N
        jTextField_medicineName.setPreferredSize(new java.awt.Dimension(70, 40));

        jTextField_medicineAvgDosage.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jTextField_medicineAvgDosage.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Gyógyszer átlagos heti adagolása", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 18), new java.awt.Color(0, 123, 255))); // NOI18N
        jTextField_medicineAvgDosage.setPreferredSize(new java.awt.Dimension(70, 40));

        jTextField_medicineExpDate.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jTextField_medicineExpDate.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Gyógyszer lejárati dátuma", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 18), new java.awt.Color(0, 123, 255))); // NOI18N
        jTextField_medicineExpDate.setPreferredSize(new java.awt.Dimension(70, 40));

        jButton_PicUp.setBackground(new java.awt.Color(255, 255, 255));
        jButton_PicUp.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jButton_PicUp.setForeground(new java.awt.Color(0, 123, 255));
        jButton_PicUp.setText("Kép feltöltése");
        jButton_PicUp.setBorder(null);
        jButton_PicUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_PicUpActionPerformed(evt);
            }
        });

        jPanel_medicineImage.setBackground(new java.awt.Color(255, 255, 255));
        jPanel_medicineImage.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Gyógyszer képe", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 18), new java.awt.Color(0, 123, 255))); // NOI18N

        jLabel_medicinePhoto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_medicinePhoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/pics/default.jpeg"))); // NOI18N

        javax.swing.GroupLayout jPanel_medicineImageLayout = new javax.swing.GroupLayout(jPanel_medicineImage);
        jPanel_medicineImage.setLayout(jPanel_medicineImageLayout);
        jPanel_medicineImageLayout.setHorizontalGroup(
                jPanel_medicineImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel_medicinePhoto, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
        );
        jPanel_medicineImageLayout.setVerticalGroup(
                jPanel_medicineImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel_medicinePhoto, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
        );

        jSpinner_medicinePieces.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jSpinner_medicinePieces.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Meglévő darabszám", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 18), new java.awt.Color(0, 123, 255))); // NOI18N
        jSpinner_medicinePieces.setVerifyInputWhenFocusTarget(false);

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(null);

        jTextArea_description.setColumns(20);
        jTextArea_description.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jTextArea_description.setRows(5);
        jTextArea_description.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Gyógyszer leírása", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 18), new java.awt.Color(0, 123, 255))); // NOI18N
        jScrollPane1.setViewportView(jTextArea_description);

        jLabel_error_name.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        jLabel_error_name.setForeground(new java.awt.Color(255, 153, 0));

        jLabel_error_avgDoses.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        jLabel_error_avgDoses.setForeground(new java.awt.Color(255, 153, 0));

        jLabel_error_expDate.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        jLabel_error_expDate.setForeground(new java.awt.Color(255, 153, 0));

        jLabel_error_currentPieces.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        jLabel_error_currentPieces.setForeground(new java.awt.Color(255, 153, 0));

        jButton_AddOrSave.setBackground(new java.awt.Color(255, 255, 255));
        jButton_AddOrSave.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jButton_AddOrSave.setForeground(new java.awt.Color(0, 123, 255));
        jButton_AddOrSave.setText("Hozzáadás");
        jButton_AddOrSave.setBorder(null);
        jButton_AddOrSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_AddOrSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel_fullLayout = new javax.swing.GroupLayout(jPanel_full);
        jPanel_full.setLayout(jPanel_fullLayout);
        jPanel_fullLayout.setHorizontalGroup(
                jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel_fullLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel_fullLayout.createSequentialGroup()
                                                .addComponent(jScrollPane1)
                                                .addContainerGap())
                                        .addGroup(jPanel_fullLayout.createSequentialGroup()
                                                .addGroup(jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(jPanel_medicineImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jButton_PicUp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addGroup(jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanel_fullLayout.createSequentialGroup()
                                                                .addGroup(jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addGroup(jPanel_fullLayout.createSequentialGroup()
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                .addGroup(jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addComponent(jTextField_medicineName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                                        .addGroup(jPanel_fullLayout.createSequentialGroup()
                                                                                                .addGap(16, 16, 16)
                                                                                                .addComponent(jLabel_error_name, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                                                        .addGroup(jPanel_fullLayout.createSequentialGroup()
                                                                                                .addComponent(jTextField_medicineAvgDosage, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                .addGap(0, 0, Short.MAX_VALUE))))
                                                                        .addGroup(jPanel_fullLayout.createSequentialGroup()
                                                                                .addGap(26, 26, 26)
                                                                                .addComponent(jLabel_error_avgDoses, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addGroup(jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addGroup(jPanel_fullLayout.createSequentialGroup()
                                                                                .addGroup(jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                                        .addComponent(jSpinner_medicinePieces, javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                        .addComponent(jLabel_error_currentPieces, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE))
                                                                                .addGap(0, 0, Short.MAX_VALUE))
                                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                                .addComponent(jTextField_medicineExpDate, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addComponent(jLabel_error_expDate, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                                .addGap(18, 18, 18))
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_fullLayout.createSequentialGroup()
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(jButton_AddOrSave, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(100, 100, 100))))))
        );
        jPanel_fullLayout.setVerticalGroup(
                jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel_fullLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanel_medicineImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanel_fullLayout.createSequentialGroup()
                                                .addGroup(jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jTextField_medicineName, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jTextField_medicineExpDate, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(6, 6, 6)
                                                .addGroup(jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(jLabel_error_expDate, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabel_error_name, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(9, 9, 9)
                                                .addGroup(jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jSpinner_medicinePieces, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jTextField_medicineAvgDosage, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(3, 3, 3)
                                                .addGroup(jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel_error_avgDoses, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabel_error_currentPieces, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jButton_AddOrSave, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jButton_PicUp, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(9, 9, 9)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)
                                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel_full, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel_full, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_PicUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_PicUpActionPerformed
        JFileChooser fc = new JFileChooser();

        int ret = fc.showSaveDialog(this);

        if (ret == JFileChooser.APPROVE_OPTION) {
            String fname = fc.getSelectedFile().getPath();
            ImageIcon ii = new ImageIcon(fname);
            jLabel_medicinePhoto.setIcon(new javax.swing.ImageIcon(ii.getImage()
                    .getScaledInstance(174, 159, Image.SCALE_DEFAULT)));

            medicine.setIconPath(fname);
        }
    }//GEN-LAST:event_jButton_PicUpActionPerformed

    private void jButton_AddOrSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_AddOrSaveActionPerformed
        String name = jTextField_medicineName.getText();
        String avgDoses = jTextField_medicineAvgDosage.getText();
        int currentPieces = ((int) jSpinner_medicinePieces.getModel().getValue());

        boolean isNameValid = isNameValid(name, jLabel_error_name);
        boolean isDoseValid = isDoseValid(avgDoses, jLabel_error_avgDoses);
        boolean isPiecesValid = isPiecesValid(currentPieces, jLabel_error_currentPieces);

        try {
            if (isNameValid && isDoseValid && isPiecesValid) {
                
                uploadPicToFolder(modifyType);
                
                if (modifyType == ModifyType.ADD) {
                    save();
                }

                finish();
            }
        } catch (ParseException e) {
            inputErrorMessage(jLabel_error_expDate, "Nem megfelelő formátum (2000-01-01)");
        }

    }//GEN-LAST:event_jButton_AddOrSaveActionPerformed

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MedicinePopup.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MedicinePopup.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MedicinePopup.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MedicinePopup.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MedicinePopup().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_AddOrSave;
    private javax.swing.JButton jButton_PicUp;
    private javax.swing.JLabel jLabel_error_avgDoses;
    private javax.swing.JLabel jLabel_error_currentPieces;
    private javax.swing.JLabel jLabel_error_expDate;
    private javax.swing.JLabel jLabel_error_name;
    private javax.swing.JLabel jLabel_medicinePhoto;
    private javax.swing.JPanel jPanel_full;
    private javax.swing.JPanel jPanel_medicineImage;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSpinner jSpinner_medicinePieces;
    private javax.swing.JTextArea jTextArea_description;
    private javax.swing.JTextField jTextField_medicineAvgDosage;

    public JLabel getjLabel_medicinePhoto() {
        return jLabel_medicinePhoto;
    }

    public JSpinner getjSpinner_medicinePieces() {
        return jSpinner_medicinePieces;
    }

    public JTextArea getjTextArea_description() {
        return jTextArea_description;
    }

    public JTextField getjTextField_medicineAvgDosage() {
        return jTextField_medicineAvgDosage;
    }

    public JTextField getjTextField_medicineExpDate() {
        return jTextField_medicineExpDate;
    }

    public JTextField getjTextField_medicineName() {
        return jTextField_medicineName;
    }

    private javax.swing.JTextField jTextField_medicineExpDate;

    private javax.swing.JTextField jTextField_medicineName;
    // End of variables declaration//GEN-END:variables

    private boolean isNameValid(String nameValue, JLabel label) {
        if (!CheckCorrection.isName(nameValue)) {
            inputErrorMessage(label, "Helytelen gyógyszernév");
            return false;
        }

        return true;
    }

    private boolean isDoseValid(String doseValue, JLabel label) {
        if (!CheckCorrection.isDose(doseValue)) {
            inputErrorMessage(label, "Helytelen dózis");
            return false;
        }

        return true;
    }

    private boolean isPiecesValid(int piecesValue, JLabel label) {
        if (piecesValue < 0) {
            inputErrorMessage(label, "Helytelen darabszám");
            return false;
        }

        return true;
    }

    private void inputErrorMessage(JLabel label, String message) {
        label.setText(message);
        label.setVisible(true);
    }

    public static String createImageName(String ext) {

        File imagesFile = new File("src/images/pics/");
        File[] filesNames = imagesFile.listFiles();
        String name = "";

        for (File act : filesNames) {
            name = act.getName().substring(0, act.getName().lastIndexOf("."));

            boolean neverUsed = false;
            do {
                String gen = Integer.toString((int) Math.round(Math.random() * 50000 + 1));

                if (!gen.equals(name)) {
                    name = gen;
                    neverUsed = true;
                }
            } while (!neverUsed);
        }

        return name + "." + ext;
    }

    private void uploadPicToFolder(ModifyType modify) throws ParseException {
        if (modify.equals(ModifyType.ADD)) {

            try {

                if ((!"src/images/pics/default.jpeg".equals(medicine.getIconPath()))) {
                    String ext = medicine.getIconPath().substring(medicine.getIconPath().lastIndexOf(".") + 1);

                    File file = new File(medicine.getIconPath());
                    File outputfile = new File("src/images/pics/" + createImageName(ext));

                    BufferedImage image = ImageIO.read(file);
                    ImageIO.write(image, ext, outputfile);
                    
                    medicine.setIconPath(outputfile.getPath());
                } else {
                    ImageIcon ii = new ImageIcon("src/images/pics/default.jpeg");
                    jLabel_medicinePhoto.setIcon(new ImageIcon(ii.getImage().getScaledInstance(174, 159, Image.SCALE_DEFAULT)));
                }

            } catch (IOException ex) {
                Logger.getLogger(MedicinePopup.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else if (modify.equals(ModifyType.MODIFY)) {
            try {
                medicine.setName(jTextField_medicineName.getText());
                medicine.setCurrentPieces((Integer) jSpinner_medicinePieces.getValue());
                medicine.setExpDate(sdf.parse(jTextField_medicineExpDate.getText()));
                medicine.setAvgDoses(Integer.parseInt(jTextField_medicineAvgDosage.getText()));
                medicine.setDescription(jTextArea_description.getText());
                medicine.setIconPath(jLabel_medicinePhoto.getIcon().toString());
            
                MedicineDao.updateMedicine(medicine, user);
            } catch (SQLException ex) {
                Logger.getLogger(MedicinePopup.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(MedicinePopup.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void save() throws ParseException {
        try {
            medicine.setName(jTextField_medicineName.getText());
            medicine.setCurrentPieces((Integer) jSpinner_medicinePieces.getValue());
            medicine.setExpDate(sdf.parse(jTextField_medicineExpDate.getText()));
            medicine.setAvgDoses(Integer.parseInt(jTextField_medicineAvgDosage.getText()));
            medicine.setDescription(jTextArea_description.getText());

            MedicineDao.saveMedicine(medicine, user);
        } catch (SQLException e) {
            LittlePopup lp = new LittlePopup("Sikertelen kapcsolat az adatbázissal!", "sqlex");
            lp.setVisible(true);
            Logger.getLogger(Medicine.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private void finish() {
        ContentScreen.updateTable(user);
        this.dispose();
    }

    private void setTextOnButton(ModifyType type) {
        if (type == ModifyType.ADD) {
            jButton_AddOrSave.setText("Hozzáadás");
        } else {
            jButton_AddOrSave.setText("Felülírás");
        }
    }

}

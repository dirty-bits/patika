package hazipatika.model;

public class User {

    private int id;

    private String email;

    private String passw;

    private String firstName;

    private String lastName;

    public User(String email, String passw, String lastName, String firstName) {
        this.email = email;
        this.passw = passw;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public User() {
        this.email = "";
        this.passw = "";
        this.firstName = "";
        this.lastName = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassw() {
        return passw;
    }

    public void setPassw(String passw) {
        this.passw = passw;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    
}

package hazipatika.view;

public class Constants {
    static final String REGISTRATION_SUCCESS = "Sikeres regisztráció";
    static final String LOG_IN_SUCCESS = "Sikeres bejelentkezés!";
    static final String PLEASE_LOG_IN = "Kérem jelentkezzen be!";

    static final String INVALID_USERNAME_OR_PASSWORD = "Nem megfelelő emailcím/jelszó páros!";
    static final String ERROR_STRING = "HIBA!";
    static final String NO_SELECTED_ROW = "Nincs sor kiválasztva!";

    public static final String NOT_ENOUGH_WARNING = "Egy hétnél kevesebb adag van ezekből:";
    public static final String EXPIRE_WITHIN_A_MONTH_WARNING = "Egy hónapon belül lejár:";

}

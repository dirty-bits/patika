package hazipatika.dao;

import hazipatika.model.Medicine;
import hazipatika.model.User;
import hazipatika.view.MedicinePopup;
import hazipatika.view.errors.Errors;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;

import javax.swing.table.DefaultTableModel;

public class MedicineDao extends BaseDao {

    public static void removeMedicine(String name) throws SQLException {

        try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password)) {
            String sqlStatement = "DELETE fk FROM fktable_felhgyogy fk INNER JOIN felhasznalo f ON fk.felhId=f.id " +
                    "INNER JOIN gyogyszer gy ON fk.gyogyId=gy.id WHERE gy.nev=?";
            PreparedStatement stmt = conn.prepareStatement(sqlStatement);

            stmt.setString(1, name);
            stmt.executeUpdate();
        }
    }

    public static String findImagePathById(int id) throws SQLException {
        try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password)) {
            String sqlStatement = "SELECT * FROM fktable_felhgyogy WHERE gyogyId=?";
            PreparedStatement stmt = conn.prepareStatement(sqlStatement);

            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return rs.getString("kepEleresiCime");
            }
        }

        return null;
    }

    public static Medicine findById(int id) throws SQLException {
        Medicine medicine = new Medicine();

        try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password)) {
            String sqlStatement = "SELECT * FROM gyogyszer WHERE id=?";
            PreparedStatement stmt = conn.prepareStatement(sqlStatement);

            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                medicine.setId(id);
                medicine.setName(rs.getString("nev"));
                medicine.setDescription(rs.getString("leiras"));
            }
        }

        return medicine;
    }

    public static void saveMedicine(Medicine m, User user) throws SQLException {


        try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password)) {

            String saveMedicine = "INSERT INTO gyogyszer (nev, leiras) VALUES(?, ?)";
            PreparedStatement medicineStmt = conn.prepareStatement(saveMedicine);

            medicineStmt.setString(1, m.getName());
            medicineStmt.setString(2, m.getDescription());
            medicineStmt.executeUpdate();

            String saveOther = "INSERT INTO fktable_felhgyogy (felhId, gyogyId, db, lejaratiDatum, atlagosAdagolas, kepEleresiCime) " +
                    "VALUES(?, LAST_INSERT_ID(), ?, ?, ?, ?)";
            PreparedStatement otherStmt = conn.prepareStatement(saveOther);

            otherStmt.setInt(1, user.getId());
            otherStmt.setInt(2, m.getCurrentPieces());
            otherStmt.setDate(3, new java.sql.Date( m.getExpDate().getTime()));
            otherStmt.setInt(4, m.getAvgDoses());
            otherStmt.setString(5, m.getIconPath());

            otherStmt.executeUpdate();
        }
    }

    public static void updateMedicine(Medicine m, User user) throws SQLException, IOException {
        PreparedStatement stmt;

        try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password)) {

            String sqlStatement = "UPDATE gyogyszer SET nev=?, leiras=? WHERE id=?";

            stmt = conn.prepareStatement(sqlStatement);
            stmt.setString(1, m.getName());
            stmt.setString(2, m.getDescription());
            stmt.setInt(3, m.getId());

            stmt.executeUpdate();
            
            String originalPath = "";
            
            String sqlStatement3 = "SELECT * FROM fktable_felhgyogy WHERE gyogyId=?";
            PreparedStatement stmt3 = conn.prepareStatement(sqlStatement3);

            stmt3.setInt(1, m.getId());
            ResultSet rs = stmt3.executeQuery();

            while(rs.next()) {
                originalPath = rs.getString("kepEleresiCime");
            }

            if(!m.getIconPath().equals(originalPath) && !m.getIconPath().equals("src/images/pics/default.jpeg")) {

                String ext = m.getIconPath().substring(m.getIconPath().lastIndexOf(".") + 1);
                
                File file = new File(m.getIconPath());
                File outputfile = new File("src/images/pics/" + MedicinePopup.createImageName(ext));
                BufferedImage image = ImageIO.read(file);
                ImageIO.write(image, ext, outputfile);

                m.setIconPath(outputfile.getPath());
            }
            
            String sqlStatement2 = "UPDATE fktable_felhgyogy SET kepEleresiCime=?, db=?, atlagosAdagolas=? WHERE felhId=? && gyogyId=?";

            PreparedStatement stmt2 = conn.prepareStatement(sqlStatement2);
            stmt2.setString(1, m.getIconPath());
            stmt2.setInt(2, m.getCurrentPieces());
            stmt2.setInt(3, m.getAvgDoses());
            stmt2.setInt(4, user.getId());
            stmt2.setInt(5, m.getId());

            stmt2.executeUpdate();

            conn.close();
        }
    }

    public static List<Integer> refreshTable(DefaultTableModel tableModel, User user) throws SQLException {
        List<Integer> idList = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password)) {

            String sqlStatement = "SELECT gyogyszer.id, nev, lejaratiDatum, db, atlagosAdagolas, leiras FROM gyogyszer " +
                    "INNER JOIN fktable_felhgyogy ON gyogyszer.id=fktable_felhgyogy.gyogyId " +
                    "INNER JOIN felhasznalo ON fktable_felhgyogy.felhId=felhasznalo.id WHERE felhasznalo.id=?";

            PreparedStatement stmt = conn.prepareStatement(sqlStatement);
            stmt.setString(1, Integer.toString(user.getId()));

            ResultSet rs = stmt.executeQuery();

            int size = 0;
            if (rs != null) {
                rs.last();
                size = rs.getRow();
            }

            if (size != 0) {

                rs.first();

                do {
                    idList.add(rs.getInt("gyogyszer.id"));
                    tableModel.addRow(
                            new Object[]{
                                    rs.getString("nev"),
                                    rs.getInt("atlagosAdagolas"),
                                    rs.getDate("lejaratiDatum"),
                                    rs.getInt("db")
                            }
                    );

                } while (rs.next());

                new Errors(tableModel);

            }
        }
        
        return idList;
    }

}

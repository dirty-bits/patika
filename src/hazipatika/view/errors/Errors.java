package hazipatika.view.errors;

import hazipatika.view.Constants;
import hazipatika.view.LittlePopup;
import hazipatika.model.Medicine;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

public class Errors {

    private DefaultTableModel table;

    public Errors(DefaultTableModel tableModel) {

        this.table = tableModel;

        checkBetween();

        checkQuantity();

    }

    private void checkBetween() {
        String drugs = "";

        for (int i = 0; i < table.getRowCount(); i++) {
            Date expDate = (Date) table.getValueAt(i, 2);

            if (betweenOneMounth(expDate)) {
                drugs += table.getValueAt(i, 0) + System.lineSeparator();
            }
        }

        if (!drugs.equals("")) {
            openBOM(drugs);
        }
    }

    private boolean betweenOneMounth(Date date) {

        try {
            String dt;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            Date currentDate = cal.getTime();
            cal.setTime(date);
            cal.add(Calendar.MONTH, -1);
            dt = sdf.format(cal.getTime());

            return sdf.parse(dt).before(sdf.parse(sdf.format(currentDate)));
        } catch (ParseException ex) {
            Logger.getLogger(Medicine.class.getName()).log(Level.SEVERE, null, ex);
        }

        return true;
    }

    private void openBOM(String drugs) {
        LittlePopup bom = new LittlePopup(Constants.EXPIRE_WITHIN_A_MONTH_WARNING, drugs);
        bom.setVisible(true);
    }

    private void checkQuantity() {

        String drugs = "";

        for (int i = 0; i < table.getRowCount(); i++) {
            int quantity = (Integer) table.getValueAt(i, 3);
            int dose = (Integer) table.getValueAt(i, 1);

            if (!isEnough(quantity, dose)) {
                drugs += table.getValueAt(i, 0) + System.lineSeparator();
            }
        }

        if (!drugs.equals("")) {
            openNotEnough(drugs);
        }

    }

    private boolean isEnough(int quantity, int dose) {
        return (dose) <= quantity;
    }

    private void openNotEnough(String drugs) {
        LittlePopup ne = new LittlePopup(Constants.NOT_ENOUGH_WARNING, drugs);
        ne.setVisible(true);
    }
}

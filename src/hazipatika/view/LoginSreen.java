package hazipatika.view;

import hazipatika.controller.InputValidator;
import hazipatika.dao.UserDao;
import hazipatika.model.User;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

public class LoginSreen extends BaseView {

    public LoginSreen() {
        initComponents();
        centeredFrame(this);
        jTextField_login_email.requestFocus();
    }

    private void openSignInPopup(java.awt.event.ActionEvent evt) {

        if (evt.getSource() == jButton_registration) {
            LittlePopup lp = new LittlePopup(Constants.REGISTRATION_SUCCESS, Constants.PLEASE_LOG_IN);
            lp.setVisible(true);
        }
    }

    private void openLogInPopup(java.awt.event.ActionEvent evt, User user) throws SQLException {
        if (evt.getSource() == jButton_login) {
            ContentScreen contentScreen = new ContentScreen(UserDao.findUserByEmailAndPassword(user));
            LittlePopup lp = new LittlePopup(Constants.LOG_IN_SUCCESS, "", contentScreen);
            lp.setVisible(true);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel_full = new javax.swing.JPanel();
        jPanel_leftside = new javax.swing.JPanel();
        jLabel_cimsor1 = new javax.swing.JLabel();
        jLabel_cimsor2_1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jTextField_reg_lastname = new javax.swing.JTextField();
        jTextField_reg_firstname = new javax.swing.JTextField();
        jTextField_reg_email = new javax.swing.JTextField();
        jPasswordField_reg_password = new javax.swing.JPasswordField();
        jButton_registration = new javax.swing.JButton();
        jLabel_error_reg_firstname = new javax.swing.JLabel();
        jLabel_error_reg_lastname = new javax.swing.JLabel();
        jLabel_error_reg_email = new javax.swing.JLabel();
        jLabel_error_reg_password = new javax.swing.JLabel();
        jLabel_logo = new javax.swing.JLabel();
        jPanel_rightside = new javax.swing.JPanel();
        jLabel_cim = new javax.swing.JLabel();
        jLabel_cimsor2_2 = new javax.swing.JLabel();
        jTextField_login_email = new javax.swing.JTextField();
        jPasswordField_login_password = new javax.swing.JPasswordField();
        jButton_login = new javax.swing.JButton();
        jLabel_error_login_email = new javax.swing.JLabel();
        jLabel_error_login_password = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Házipatika");
        setName("frame_login"); // NOI18N
        setResizable(false);

        jPanel_full.setBackground(new java.awt.Color(255, 255, 255));

        jPanel_leftside.setBackground(new java.awt.Color(255, 255, 255));

        jLabel_cimsor1.setFont(new java.awt.Font("Arial", 0, 36)); // NOI18N
        jLabel_cimsor1.setText("Házipatika");
        jLabel_cimsor1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel_cimsor1MouseClicked(evt);
            }
        });

        jLabel_cimsor2_1.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel_cimsor2_1.setText("Hozza létre a saját házipatikáját...");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jTextField_reg_lastname.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jTextField_reg_lastname.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Vezetéknév", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 18), new java.awt.Color(0, 123, 255))); // NOI18N
        jTextField_reg_lastname.setPreferredSize(new java.awt.Dimension(70, 40));
        jTextField_reg_lastname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField_reg_lastnameFocusGained(evt);
            }
        });

        jTextField_reg_firstname.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jTextField_reg_firstname.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Keresztnév", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 18), new java.awt.Color(0, 123, 255))); // NOI18N
        jTextField_reg_firstname.setPreferredSize(new java.awt.Dimension(70, 40));
        jTextField_reg_firstname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField_reg_firstnameFocusGained(evt);
            }
        });

        jTextField_reg_email.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jTextField_reg_email.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "E-mail", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 18), new java.awt.Color(0, 123, 255))); // NOI18N
        jTextField_reg_email.setPreferredSize(new java.awt.Dimension(70, 40));
        jTextField_reg_email.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField_reg_emailFocusGained(evt);
            }
        });

        jPasswordField_reg_password.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jPasswordField_reg_password.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Jelszó", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 18), new java.awt.Color(0, 123, 255))); // NOI18N
        jPasswordField_reg_password.setPreferredSize(new java.awt.Dimension(70, 40));
        jPasswordField_reg_password.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jPasswordField_reg_passwordFocusGained(evt);
            }
        });
        jPasswordField_reg_password.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jPasswordField_reg_passwordKeyReleased(evt);
            }
        });

        jButton_registration.setBackground(new java.awt.Color(255, 255, 255));
        jButton_registration.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jButton_registration.setForeground(new java.awt.Color(0, 123, 255));
        jButton_registration.setText("Regisztráció");
        jButton_registration.setBorder(null);
        jButton_registration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_registrationActionPerformed(evt);
            }
        });

        jLabel_error_reg_firstname.setBackground(new java.awt.Color(255, 255, 255));
        jLabel_error_reg_firstname.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel_error_reg_firstname.setForeground(new java.awt.Color(255, 153, 0));
        jLabel_error_reg_firstname.setFocusable(false);

        jLabel_error_reg_lastname.setBackground(new java.awt.Color(255, 255, 255));
        jLabel_error_reg_lastname.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel_error_reg_lastname.setForeground(new java.awt.Color(255, 153, 0));
        jLabel_error_reg_lastname.setFocusable(false);

        jLabel_error_reg_email.setBackground(new java.awt.Color(255, 255, 255));
        jLabel_error_reg_email.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel_error_reg_email.setForeground(new java.awt.Color(255, 153, 0));
        jLabel_error_reg_email.setFocusable(false);

        jLabel_error_reg_password.setBackground(new java.awt.Color(255, 255, 255));
        jLabel_error_reg_password.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel_error_reg_password.setForeground(new java.awt.Color(255, 153, 0));
        jLabel_error_reg_password.setFocusable(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addContainerGap(214, Short.MAX_VALUE)
                                .addComponent(jButton_registration, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(204, 204, 204))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jTextField_reg_lastname, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jTextField_reg_firstname, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addComponent(jTextField_reg_email, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                .addGap(10, 10, 10)
                                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                        .addComponent(jLabel_error_reg_email, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(jLabel_error_reg_lastname, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                                .addGap(28, 28, 28)
                                                                .addComponent(jLabel_error_reg_password, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                .addGap(28, 28, 28)
                                                                .addComponent(jLabel_error_reg_firstname, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jPasswordField_reg_password, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))))))
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextField_reg_lastname, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextField_reg_firstname, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel_error_reg_firstname, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel_error_reg_lastname, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextField_reg_email, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPasswordField_reg_password, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel_error_reg_email, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel_error_reg_password, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jButton_registration, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel_logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N

        javax.swing.GroupLayout jPanel_leftsideLayout = new javax.swing.GroupLayout(jPanel_leftside);
        jPanel_leftside.setLayout(jPanel_leftsideLayout);
        jPanel_leftsideLayout.setHorizontalGroup(
                jPanel_leftsideLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel_leftsideLayout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(jLabel_logo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel_leftsideLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel_cimsor2_1)
                                        .addComponent(jLabel_cimsor1))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel_leftsideLayout.setVerticalGroup(
                jPanel_leftsideLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel_leftsideLayout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addGroup(jPanel_leftsideLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanel_leftsideLayout.createSequentialGroup()
                                                .addComponent(jLabel_cimsor1)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel_cimsor2_1))
                                        .addComponent(jLabel_logo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(39, 39, 39)
                                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(39, 39, 39))
        );

        jPanel_rightside.setBackground(new java.awt.Color(0, 123, 255));

        jLabel_cim.setFont(new java.awt.Font("Arial", 0, 36)); // NOI18N
        jLabel_cim.setForeground(new java.awt.Color(255, 255, 255));
        jLabel_cim.setText("Bejelentkezés");

        jLabel_cimsor2_2.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel_cimsor2_2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel_cimsor2_2.setText("Hozza létre a saját házipatikáját...");

        jTextField_login_email.setBackground(new java.awt.Color(0, 123, 255));
        jTextField_login_email.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jTextField_login_email.setForeground(new java.awt.Color(255, 255, 255));
        jTextField_login_email.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "E-mail", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 18), new java.awt.Color(255, 255, 255))); // NOI18N
        jTextField_login_email.setPreferredSize(new java.awt.Dimension(70, 40));
        jTextField_login_email.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField_login_emailFocusGained(evt);
            }
        });

        jPasswordField_login_password.setBackground(new java.awt.Color(0, 123, 255));
        jPasswordField_login_password.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jPasswordField_login_password.setForeground(new java.awt.Color(255, 255, 255));
        jPasswordField_login_password.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Jelszó", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 18), new java.awt.Color(255, 255, 255))); // NOI18N
        jPasswordField_login_password.setPreferredSize(new java.awt.Dimension(70, 40));
        jPasswordField_login_password.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jPasswordField_login_passwordFocusGained(evt);
            }
        });
        jPasswordField_login_password.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jPasswordField_login_passwordKeyReleased(evt);
            }
        });

        jButton_login.setBackground(new java.awt.Color(0, 123, 255));
        jButton_login.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jButton_login.setForeground(new java.awt.Color(255, 255, 255));
        jButton_login.setText("Bejelentkezés");
        jButton_login.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 123, 255), 1, true));
        jButton_login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_loginActionPerformed(evt);
            }
        });

        jLabel_error_login_email.setBackground(new java.awt.Color(255, 255, 255));
        jLabel_error_login_email.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel_error_login_email.setForeground(new java.awt.Color(255, 153, 0));
        jLabel_error_login_email.setFocusable(false);

        jLabel_error_login_password.setBackground(new java.awt.Color(255, 255, 255));
        jLabel_error_login_password.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel_error_login_password.setForeground(new java.awt.Color(255, 153, 0));
        jLabel_error_login_password.setFocusable(false);

        javax.swing.GroupLayout jPanel_rightsideLayout = new javax.swing.GroupLayout(jPanel_rightside);
        jPanel_rightside.setLayout(jPanel_rightsideLayout);
        jPanel_rightsideLayout.setHorizontalGroup(
                jPanel_rightsideLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel_rightsideLayout.createSequentialGroup()
                                .addContainerGap(50, Short.MAX_VALUE)
                                .addGroup(jPanel_rightsideLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_rightsideLayout.createSequentialGroup()
                                                .addComponent(jButton_login, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(122, 122, 122))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_rightsideLayout.createSequentialGroup()
                                                .addGroup(jPanel_rightsideLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabel_error_login_password, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(jPanel_rightsideLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                .addComponent(jLabel_cimsor2_2)
                                                                .addComponent(jLabel_cim)
                                                                .addComponent(jTextField_login_email, javax.swing.GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE)
                                                                .addComponent(jPasswordField_login_password, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(jLabel_error_login_email, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGap(50, 50, 50))))
        );
        jPanel_rightsideLayout.setVerticalGroup(
                jPanel_rightsideLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel_rightsideLayout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addComponent(jLabel_cim)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel_cimsor2_2)
                                .addGap(43, 43, 43)
                                .addComponent(jTextField_login_email, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(1, 1, 1)
                                .addComponent(jLabel_error_login_email, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jPasswordField_login_password, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(1, 1, 1)
                                .addComponent(jLabel_error_login_password, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton_login, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel_fullLayout = new javax.swing.GroupLayout(jPanel_full);
        jPanel_full.setLayout(jPanel_fullLayout);
        jPanel_fullLayout.setHorizontalGroup(
                jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel_fullLayout.createSequentialGroup()
                                .addComponent(jPanel_leftside, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanel_rightside, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel_fullLayout.setVerticalGroup(
                jPanel_fullLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel_leftside, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jPanel_rightside, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel_full, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel_full, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel_cimsor1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel_cimsor1MouseClicked

        if (evt.getClickCount() == 5) {

            JFrame easterEgg = new EasterEgg();
            easterEgg.setVisible(true);
        }
    }//GEN-LAST:event_jLabel_cimsor1MouseClicked

    private void jButton_loginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_loginActionPerformed

        boolean isEmail = InputValidator.isEmailValid(jTextField_login_email.getText(), jLabel_error_login_email);
        boolean isPassword = InputValidator.isPasswordValid(jPasswordField_login_password, jLabel_error_login_password);
        User user = new User();
        user.setEmail(jTextField_login_email.getText());
        user.setPassw(String.valueOf(jPasswordField_login_password.getPassword()));

        if (isEmail && isPassword) {

            try {
                if (UserDao.checkLogin(user)) {
                    openLogInPopup(evt, user);
                    this.dispose();
                } else {
                    LittlePopup lp = new LittlePopup(Constants.INVALID_USERNAME_OR_PASSWORD, "");
                    lp.setVisible(true);
                    jTextField_login_email.setText("");
                    jPasswordField_login_password.setText("");
                }
            } catch (SQLException e) {
                LittlePopup lp = new LittlePopup("Sikertelen kapcsolat az adatbázissal!", "sqlex");
                lp.setVisible(true);
            }
        }

    }//GEN-LAST:event_jButton_loginActionPerformed

    private void jButton_registrationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_registrationActionPerformed

        if (InputValidator.isNameValid(jTextField_reg_lastname.getText(), jLabel_error_reg_lastname)
                && InputValidator.isNameValid(jTextField_reg_firstname.getText(), jLabel_error_reg_firstname)
                && InputValidator.isEmailValid(jTextField_reg_email.getText(), jLabel_error_reg_email)
                && InputValidator.isPasswordValid(jPasswordField_reg_password, jLabel_error_reg_password)) {

            User u = new User(jTextField_reg_email.getText(), String.valueOf(jPasswordField_reg_password.getPassword()),
                    jTextField_reg_lastname.getText(), jTextField_reg_firstname.getText());

            try {
                UserDao.saveUser(u);
                openSignInPopup(evt);
            } catch (SQLException e) {
                LittlePopup lp = new LittlePopup("Sikertelen kapcsolat az adatbázissal!", "sqlex");
                lp.setVisible(true);
                Logger.getLogger(MedicinePopup.class.getName()).log(Level.SEVERE, null, e);
            }

            jTextField_reg_email.setText("");
            jTextField_reg_firstname.setText("");
            jTextField_reg_lastname.setText("");
            jPasswordField_reg_password.setText("");
        }

    }//GEN-LAST:event_jButton_registrationActionPerformed

    private void jTextField_login_emailFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_login_emailFocusGained
        jLabel_error_login_email.setText("");
    }//GEN-LAST:event_jTextField_login_emailFocusGained

    private void jPasswordField_login_passwordFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jPasswordField_login_passwordFocusGained
        jLabel_error_login_password.setText("");
    }//GEN-LAST:event_jPasswordField_login_passwordFocusGained

    private void jTextField_reg_lastnameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_reg_lastnameFocusGained
        jLabel_error_reg_lastname.setText("");

    }//GEN-LAST:event_jTextField_reg_lastnameFocusGained

    private void jTextField_reg_firstnameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_reg_firstnameFocusGained
        // TODO add your handling code here:
        jLabel_error_reg_firstname.setText("");
    }//GEN-LAST:event_jTextField_reg_firstnameFocusGained

    private void jTextField_reg_emailFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_reg_emailFocusGained
        jLabel_error_reg_email.setText("");
    }//GEN-LAST:event_jTextField_reg_emailFocusGained

    private void jPasswordField_reg_passwordFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jPasswordField_reg_passwordFocusGained
        jLabel_error_reg_password.setText("");
    }//GEN-LAST:event_jPasswordField_reg_passwordFocusGained

    private void jPasswordField_login_passwordKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPasswordField_login_passwordKeyReleased

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jButton_login.doClick();
        }
    }//GEN-LAST:event_jPasswordField_login_passwordKeyReleased

    private void jPasswordField_reg_passwordKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPasswordField_reg_passwordKeyReleased

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jButton_registration.doClick();
        }
    }//GEN-LAST:event_jPasswordField_reg_passwordKeyReleased

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginSreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginSreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginSreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginSreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new LoginSreen().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_login;
    private javax.swing.JButton jButton_registration;
    private javax.swing.JLabel jLabel_cim;
    private javax.swing.JLabel jLabel_cimsor1;
    private javax.swing.JLabel jLabel_cimsor2_1;
    private javax.swing.JLabel jLabel_cimsor2_2;
    private javax.swing.JLabel jLabel_error_login_email;
    private javax.swing.JLabel jLabel_error_login_password;
    private javax.swing.JLabel jLabel_error_reg_email;
    private javax.swing.JLabel jLabel_error_reg_firstname;
    private javax.swing.JLabel jLabel_error_reg_lastname;
    private javax.swing.JLabel jLabel_error_reg_password;
    private javax.swing.JLabel jLabel_logo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel_full;
    private javax.swing.JPanel jPanel_leftside;
    private javax.swing.JPanel jPanel_rightside;
    private javax.swing.JPasswordField jPasswordField_login_password;
    private javax.swing.JPasswordField jPasswordField_reg_password;
    private javax.swing.JTextField jTextField_login_email;
    private javax.swing.JTextField jTextField_reg_email;
    private javax.swing.JTextField jTextField_reg_firstname;
    private javax.swing.JTextField jTextField_reg_lastname;
    // End of variables declaration//GEN-END:variables
}
